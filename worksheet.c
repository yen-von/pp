#include<stdio.h>
#include<math.h>
#include"omp.h"
int main(){
	int i,j,n=1000000;
	double s=0.0,a=0,x=0,timea,timeb;
	for(j=0;j<10;j++){
		omp_set_num_threads(j);
		printf("thread num:%d\n",j);

		timeb=omp_get_wtime();
#pragma omp parallel for private(x)
		for(i=0;i<n;i++){
#pragma omp critical
			{
				x=(i+0.5)/n;
				a += 4.0 / (1.0+x*x);
			}
		}
		//printf("  %.10lf\n",a/n);
		a=0;
		timea=omp_get_wtime();
		printf("  %lf\n",timea-timeb);
#pragma omp parallel for private(x)
		for(i=0;i<n;i++){
			{
				x=(i+0.5)/n;
#pragma omp critical
				a += 4.0 / (1.0+x*x);
			}
		}
		//printf("  %.10lf\n",a/n);
		timeb=omp_get_wtime();
		a=0;
		printf("  %lf\n",timeb-timea);
#pragma omp parallel for private(x) reduction(+:a)
		for(i=0;i<n;i++){
			{
				x=(i+0.5)/n;
				a += 4.0 / (1.0+x*x);
			}
		}
		//printf("  %.10lf\n",a/n);
		timea=omp_get_wtime();
		printf("  %lf\n",timea-timeb);
	}
	return 0;
}
